/* Copyright (c) 2018, Jesper Öqvist <jesper.oqvist@cs.lth.se>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package org.extendj.ragdoc;

import se.llbit.io.LookaheadReader;
import se.llbit.json.JsonArray;
import se.llbit.json.JsonObject;
import se.llbit.json.JsonParser;

import java.io.Closeable;
import java.io.IOException;
import java.io.StringReader;

public class AstDeclParser implements AutoCloseable, Closeable {
  private static final int EOF = -1;

  private final LookaheadReader in;

  public AstDeclParser(String astdecl) {
    in = new LookaheadReader(new StringReader(astdecl), 2);
  }

  private void accept(char c) throws IOException, JsonParser.SyntaxError {
    int next = in.pop();
    if (next == EOF) {
      throw new JsonParser.SyntaxError(String.format("unexpected end of input (expected '%c')", c));
    }
    if (next != c) {
      throw new JsonParser.SyntaxError(
          String.format("unexpected character (was '%c', expected '%c')", (char) next, c));
    }
  }

  JsonObject parse() throws IOException, JsonParser.SyntaxError {
    JsonObject decl = new JsonObject();
    String name = parseName();
    decl.add("n", name);
    skipWhitespace();
    if (in.peek() == ':' && in.peek(1) != ':') {
      accept(':');
      skipWhitespace();
      String extendsName = parseName();
      decl.add("e", extendsName);
      skipWhitespace();
    }
    if (in.peek() != ';') {
      accept(':');
      accept(':');
      accept('=');
      skipWhitespace();
      JsonArray components = new JsonArray();
      while (in.peek() != ';') {
        components.add(parseComponent());
        skipWhitespace();
      }
      if (!components.isEmpty()) {
        decl.add("c", components);
      }
    }
    accept(';');
    return decl;
  }

  private JsonObject parseComponent() throws IOException, JsonParser.SyntaxError {
    JsonObject component = new JsonObject();
    if (in.peek() == '[') {
      accept('[');
      skipWhitespace();
      String name = parseName();

      skipWhitespace();
      if (in.peek() == ':') {
        accept(':');
        skipWhitespace();
        String extendsName = parseName();
        component.add("n", name);
        component.add("e", extendsName);
        skipWhitespace();
      } else {
        component.add("e", name);
      }

      component.add("k", "opt");

      skipWhitespace();
      accept(']');
      return component;
    } else if (in.peek() == '<') {
      accept('<');
      skipWhitespace();
      String name = parseName();
      component.add("n", name);

      skipWhitespace();
      if (in.peek() == ':') {
        accept(':');
        skipWhitespace();
        // allow generic type for tokens
        String extendsName = parseName(true);
        component.add("e", extendsName);
        skipWhitespace();
      } else {
        component.add("e", "String");
      }

      component.add("k", "token");

      skipWhitespace();
      accept('>');
      return component;
    } else {
      String name = parseName();

      skipWhitespace();
      if (in.peek() == ':') {
        accept(':');
        skipWhitespace();
        String extendsName = parseName();
        component.add("n", name);
        component.add("e", extendsName);
        skipWhitespace();
      } else {
        component.add("e", name);
      }

      if (in.peek() == '*') {
        accept('*');
        component.add("k", "list");
      }
      return component;
    }
  }

  private void skipWhitespace() throws IOException {
    while (isWhitespace(in.peek())) {
      in.pop();
    }
  }

  private boolean isWhitespace(int chr) {
    return chr == 0x20 || chr == 0x09 || chr == 0x0A || chr == 0x0D;
  }

  private boolean isNameChar(int chr) {
     return chr != EOF && (chr == '.' || Character.isLetterOrDigit(chr) || chr == '_');
  }

  private String chrToStr(int chr) {
    return chr != EOF ? String.format("%c", chr) : "EOF";
  }

  private String parseName() throws IOException, JsonParser.SyntaxError {
    return parseName(false);
  }

  private String parseName(boolean allowGeneric) throws IOException, JsonParser.SyntaxError {
    int next = in.peek();
    if (!isNameChar(next)) {
      throw new JsonParser.SyntaxError(
          String.format("Error: expected name or typename, found %s", chrToStr(next)));
    }
    StringBuilder name = new StringBuilder();
    while (true) {
      int peek = in.peek();
      if (allowGeneric && peek == '<') {
        // add everything until closing '>' is found, and then break
        while (in.peek() != '>') {
          name.append((char) in.pop());
        }
        // also add final '>'
        name.append((char) in.pop());
        break;
      }
      if (!isNameChar(peek)) {
        break;
      }
      name.append((char) in.pop());
    }
    return name.toString();
  }

  @Override public void close() throws IOException {
    in.close();
  }
}
