/* Copyright (c) 2013-2017, Jesper Öqvist <jesper.oqvist@cs.lth.se>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package org.extendj.util;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Utility class for calculating relative file paths.
 * 
 * @author Jesper Öqvist <jesper.oqvist@cs.lth.se>
 * 
 */
public class RelativePath {

	private static final String SEPARATOR = "/";

	private static final List<String> cwd;
	
	static {
		File dir = new File(System.getProperty("user.dir"));

		if (dir.isDirectory()) {
			cwd = buildPathList(dir);
		} else {
			cwd = new LinkedList<String>();
			System.err.println(
					"Error: Could not find current working directory");
		}
	}

	/**
	 * Builds the path list used to compute relative paths.
	 * @return The path list for the given path
	 */
	public static List<String> buildPathList(File path) {
		List<String> parts = new ArrayList<String>();

		try {
			path = path.getCanonicalFile();
			while (path != null) {
				parts.add(0, path.getName());
				path = path.getParentFile();
			}
		} catch (IOException e) {
			return new LinkedList<String>();
		}

		return parts;
	}

	/**
	 * Returns a string representation of the relative path for
	 * the path <code>path</code> from the current working directory.
	 * @param path The original path we want a relative path for
	 * @return The string representing the relative path that corresponds
	 * to the absolute path
	 */
	public static String getRelativePath(String path) {
		return getRelativePath(path, cwd);
	}
	
	/**
	 * Compute the relative path to a file from a base path
	 * @param base The path list for the base path
	 * @return The relative path to the file at a given path
	 */
	public static String getRelativePath(String path, List<String> base) {
		StringBuilder str = new StringBuilder();
		List<String> leaf = buildPathList(new File(path));

		int I = base.size();
		int J = leaf.size();
		int n = 0;

		while (n < I && n < J && base.get(n).equals(leaf.get(n))) {
			n += 1;
		}

		int o = n;
		while (o < I) {
			str.append(".." + SEPARATOR);
			o += 1;
		}

		while (n < J) {
			str.append(leaf.get(n));

			n += 1;
			if (n < J)
				str.append(SEPARATOR);
		}
		return str.toString();
	}
}
