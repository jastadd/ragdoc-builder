# RagDoc Builder

This is a tool for building documentation metadata for JastAdd projects.
RD-Builder outputs JSON files with documentation metadata.  The documentation
can be viewed with RagDoc Viewer.


## Building

Build the tool with Gradle:

    ./gradlew fatJar
    java -jar rd-builder.jar -version


## Usage

Run RagDoc Builder on the generated JastAdd code, just like a regular Java compiler.
The output is written to the path specified by the `-d` option:

    java -jar rd-builder.jar -d docs -classpath $CLASSPATH File1.java ...


The documentation is generated as a set of `.json` files containing information about
the classes and attributes in the input sources. This information can be viewed on
a webpage using RagDoc Viewer.

RagDoc Builder can also be run using JastAddGradle. By running RagDoc Builder
this way, you won't have to set up the full compile path because JastAddGradle
knows how to compile the generated JastAdd code.
