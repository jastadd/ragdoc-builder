FROM openjdk:8
WORKDIR /ragdoc-builder
COPY gradle /ragdoc-builder/gradle
COPY gradlew /ragdoc-builder/
RUN chmod +x ./gradlew && ./gradlew --no-daemon --version
ADD . /ragdoc-builder/
RUN chmod +x start-builder.sh
RUN ./gradlew --no-daemon fatJar && rm -r gradle/ extendj/ src/ build/ gradlew build.gradle settings.gradle
ENV BOOT_CLASSPATH="/usr/local/openjdk-8/jre/lib/rt.jar"
VOLUME ["/src"]
VOLUME ["/data"]
ENTRYPOINT [""]
