#!/bin/bash

set -eu

OUTDIR="${1:-data}"

echo "OUTDIR: ${OUTDIR}"

./gradlew fatjar
java -jar rd-builder.jar \
	$(find src/gen/ -name '*.java') \
	$(find extendj/src/frontend -name '*.java') \
	-d "${OUTDIR}"
